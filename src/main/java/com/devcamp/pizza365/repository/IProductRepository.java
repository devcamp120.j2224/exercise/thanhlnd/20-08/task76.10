package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Product;

public interface IProductRepository extends JpaRepository<Product,Long>{
    @Query(value = "SELECT * FROM products WHERE product_code LIKE :productcode%", nativeQuery = true)
    List<Product> findByProductCode(@Param("productcode") String productcode);

    @Query(value = "SELECT * FROM products WHERE product_name LIKE :productname%", nativeQuery = true)
    List<Product> findByProductName(@Param("productname") String productname);

	@Transactional
	@Modifying
	@Query(value = "UPDATE products SET product_vendor = :productvendor  WHERE product_vendor Is Null", nativeQuery = true)
	int updateProductVendor(@Param("productvendor") String productvendor);
}


package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.COrder;

@Repository
public interface IOrderRepository extends JpaRepository<COrder, Long> {
	COrder findByOrderCode(String orderId);

	@Query(value = "SELECT * FROM orders WHERE id LIKE :id%", nativeQuery = true)
    List<COrder> findById(@Param("id") Integer id);

    @Query(value = "SELECT * FROM orders WHERE status LIKE :status%", nativeQuery = true)
    List<COrder> findByStatusLike(@Param("status") String status);

	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET comments = :comment  WHERE comments = 'null'", nativeQuery = true)
	int updateComment(@Param("comment") String comment);
}

package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentRepository extends JpaRepository <Payment,Long>{
    

    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :checknumber%", nativeQuery = true)
    List<Payment> findByCheckNumberLike(@Param("checknumber") String checknumber);

    @Query(value = "SELECT * FROM payments WHERE customer_id LIKE :id%", nativeQuery = true)
    List<Payment> findByCustomerIdLike(@Param("id") Integer id);

}
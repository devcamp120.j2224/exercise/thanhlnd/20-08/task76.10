package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Employee;

public interface IEmployeeRepository extends JpaRepository<Employee,Long>{
    
    @Query(value = "SELECT * FROM employees WHERE first_name LIKE :firstName%", nativeQuery = true)
	List<Employee> findByFirstNameLike(@Param("firstName") String firstName);

    @Query(value = "SELECT * FROM employees WHERE last_name LIKE :lastName%", nativeQuery = true)
	List<Employee> findByLastNameLike(@Param("lastName") String lastName);

	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET job_title = :jobtitle  WHERE job_title Is Null", nativeQuery = true)
	int updateJobTitle(@Param("jobtitle") String jobtitle);
}


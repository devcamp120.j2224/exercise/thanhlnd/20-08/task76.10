package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Office;

public interface IOfficeRepository extends JpaRepository<Office,Long> {
    @Query(value = "SELECT * FROM offices WHERE city LIKE :city%", nativeQuery = true)
	List<Office> findByCityLike(@Param("city") String city);

    @Query(value = "SELECT * FROM offices WHERE state LIKE :state%", nativeQuery = true)
	List<Office> findByStateLike(@Param("state") String state);

	@Transactional
	@Modifying
	@Query(value = "UPDATE offices SET country = :country  WHERE country = 'null'", nativeQuery = true)
	int updateCountry(@Param("country") String country);

    @Query(value = "UPDATE offices SET state = :state  WHERE state = 'null'", nativeQuery = true)
	int updateState(@Param("state") String state);
}

package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.OrderDetail;

public interface IOrderDetailRepository extends JpaRepository <OrderDetail,Long> {
    
    @Query(value = "SELECT * FROM order_details WHERE order_id LIKE :orderid%", nativeQuery = true)
    List<OrderDetail> findByOrderId(@Param("orderid") Integer orderid);

    @Query(value = "SELECT * FROM order_details WHERE quantity_order LIKE :quantityorder%", nativeQuery = true)
    List<OrderDetail> findByQuantityOrder(@Param("quantityorder") Integer quantityorder);

	@Transactional
	@Modifying
	@Query(value = "UPDATE order_details SET price_each = :price  WHERE price_each Is Null", nativeQuery = true)
	int updatePriceEach(@Param("price") String price);
}

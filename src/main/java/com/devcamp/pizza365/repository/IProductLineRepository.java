package com.devcamp.pizza365.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.ProductLine;

public interface IProductLineRepository extends JpaRepository<ProductLine,Long>{
    @Query(value = "SELECT * FROM product_lines WHERE product_line LIKE :productline%", nativeQuery = true)
    List<ProductLine> findByProductLine(@Param("productline") String productline);

    @Query(value = "SELECT * FROM product_lines WHERE description LIKE :description%", nativeQuery = true)
    List<ProductLine> findByDiscription(@Param("description") String description);

}


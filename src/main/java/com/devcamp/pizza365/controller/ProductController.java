package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;

import java.util.ArrayList;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
    @Autowired
    IProductRepository iProductRepository;

    @Autowired
    IProductLineRepository iProductLineRepository;

    // tìm all product
    @GetMapping("products/all")
    public ResponseEntity<List<Product>> getAllProduct() {
        try {
            List<Product> allProduct = new ArrayList<Product>();

            iProductRepository.findAll().forEach(allProduct::add);

            return new ResponseEntity<>(allProduct, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //tìm product bằng id
    @GetMapping("products/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable("id") long id) {
		Optional<Product> productData = iProductRepository.findById(id);
		if (productData.isPresent()) {
			return new ResponseEntity<>(productData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    //tạo mới product
    @PostMapping("/product-line/{id}/createproduct")
    public ResponseEntity<Object> createProduct (@PathVariable("id") Long id,@RequestBody Product product) {
        try {
            Optional<ProductLine> checkProductLine = iProductLineRepository.findById(id);
            if(checkProductLine.isPresent()){
                Product dataProduct = new Product();
                dataProduct.setProductCode(product.getProductCode());
                dataProduct.setProductName(product.getProductName());
                dataProduct.setProductDescripttion(product.getProductDescripttion());
                dataProduct.setProductScale(product.getProductScale());
                dataProduct.setProductVendor(product.getProductVendor());
                dataProduct.setQuantityInStock(product.getQuantityInStock());
                dataProduct.setBuyPrice(product.getBuyPrice());
                dataProduct.setProductLine(checkProductLine.get());

                
                
                return new ResponseEntity<>(iProductRepository.save(dataProduct),HttpStatus.CREATED);
            } else {
                return ResponseEntity.unprocessableEntity().body("Không tìm thấy Product Line");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //update product bằng id
    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProductById(@PathVariable("id")long id,@RequestBody Product product) {
        try {
            Optional<Product> checkProductId = iProductRepository.findById(id);
            if(checkProductId.isPresent()) {
                Product updateProduct = checkProductId.get();
                updateProduct.setProductCode(product.getProductCode());
                updateProduct.setProductName(product.getProductName());
                updateProduct.setProductDescripttion(product.getProductDescripttion());
                updateProduct.setProductScale(product.getProductScale());
                updateProduct.setProductVendor(product.getProductVendor());
                updateProduct.setQuantityInStock(product.getQuantityInStock());
                updateProduct.setBuyPrice(product.getBuyPrice());

                return new ResponseEntity<Object>(iProductRepository.save(updateProduct), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 


    //xoá product bằng id
    @DeleteMapping("/products/{productId}")
    public ResponseEntity<Product> deleteByProductId(@PathVariable("productId") long productId){
        try {
            iProductRepository.deleteById(productId);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
